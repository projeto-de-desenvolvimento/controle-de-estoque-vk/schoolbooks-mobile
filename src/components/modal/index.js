import React from 'react'
import './styles.css'

const Modal = ({ handleClose, show, title, message, children }) => {
	const showHideClassName = show ? 'modal display-block' : 'modal display-none'

	return (
		<div className={showHideClassName}>
			<div className="modal-main">
				<div className="modal-title">{title || 'Você se registrou com sucesso.'}</div>
				<hr className="modal-hr" />
				<div className="modal-content">
					{message ||
						'Seja bem-vindo a Conig, agora você é um de nossos membros, qualquer dúvida contacte-nos.'}
				</div>
				<button type="button" className="modal-button" onClick={handleClose}>
					Ok
				</button>
			</div>
		</div>
	)
}

export default Modal
