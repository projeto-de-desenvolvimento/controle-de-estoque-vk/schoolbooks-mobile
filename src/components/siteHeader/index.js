import React, { useEffect, useState } from 'react'
import jwt_decode from 'jwt-decode'
import './styles.css'
import { Link, Redirect, useHistory } from 'react-router-dom'
import logo_conig from '../../assets/images/logo.png'
import DropDownCard from '../dropdown/dropdownCard'
import Button from '../dropdown/button'
import api from '../../services/api'
import SiteHeaderLogged from '../siteHeaderLogged'

function SiteHeader(props) {
	const [isLogged, setLogged] = useState(false)
	const [student, setStudent] = useState({})
	const [redirect, setRedirect] = useState(false)
	const history = useHistory()
	async function getStudent() {
		setLogged(true)
	}
	useEffect(() => {
		if (localStorage.getItem('@conig-Token')) {
			getStudent()
		}
	}, [])

	const profileLinks = new Array(
		{ name: 'Perfil', link: '/profile' },
		{ name: 'Sair', link: '/logout' }
	)

	return (
		<div className="w-full text-gray-700 bg-white-700 dark-mode:text-gray-200 dark-mode:bg-gray-800">
			{redirect && <Redirect to="/signin" />}
			{!isLogged ? (
				<>
					<nav className="w-full h-18 z-30 top-10 py-1 shadow-lg border-b-4 border-yellow-500 navigation">
						<div className="flex flex-col w-full px-4 md:flex-row md:px-6 lg:px-8">
							<Link
								to="/initial-page"
								className="md:mt-o md:pt-0 md:mx-0 place-self-center md:mr-auto"
							>
								<img src={logo_conig} alt="Logo Conig" className="w-25 logo" />
							</Link>
							<ul className="flex flex-col mt-4 -mx-4 pt-4 border-t md:flex-row md:items-center md:mx-0 md:ml-auto md:mt-0 md:pt-0 md:border-0">
								<li>
									<Link
										to="/courses"
										className="block px-6 py-2 text-xs font-medium leading-6 text-center mobile text-white uppercase transition bg-yellow-600 rounded-full shadow ripple hover:shadow-lg hover:bg-yellow-500 focus:outline-none float-right m-6"
									>
										Módulos
										<svg
											className="w-6 h-6 float-right md:ml-2"
											fill="none"
											stroke="currentColor"
											viewBox="0 0 24 24"
											xmlns="http://www.w3.org/2000/svg"
										>
											<path
												strokeLinecap="round"
												strokeLinejoin="round"
												strokeWidth="2"
												d="M12 6.253v13m0-13C10.832 5.477 9.246 5 7.5 5S4.168 5.477 3 6.253v13C4.168 18.477 5.754 18 7.5 18s3.332.477 4.5 1.253m0-13C13.168 5.477 14.754 5 16.5 5c1.747 0 3.332.477 4.5 1.253v13C19.832 18.477 18.247 18 16.5 18c-1.746 0-3.332.477-4.5 1.253"
											/>
										</svg>
									</Link>
								</li>
								<li>
									<Link
										to="/ranking"
										className="block px-6 py-2 text-xs font-medium leading-6 text-center mobile text-white uppercase transition bg-yellow-600 rounded-full shadow ripple hover:shadow-lg hover:bg-yellow-500 focus:outline-none float-right m-6"
									>
										Ranking
									</Link>
								</li>
								<li />
								<li>
									<Link
										to="/signin"
										className="block px-6 py-2 text-xs font-medium leading-6 text-center mobile text-white uppercase transition bg-yellow-600 rounded-full shadow ripple hover:shadow-lg hover:bg-yellow-500 focus:outline-none float-right m-6"
									>
										Entrar
									</Link>
								</li>
								<li>
									<Link
										to="/signup"
										className="block px-6 signup py-2 text-xs font-medium leading-6 text-center mobile text-white uppercase transition bg-yellow-600 rounded-full shadow ripple hover:shadow-lg hover:bg-yellow-500 focus:outline-none float-right m-6"
									>
										Registrar-se
									</Link>
								</li>
							</ul>
						</div>
					</nav>
				</>
			) : (
				<SiteHeaderLogged />
			)}
		</div>
	)
}

export default SiteHeader
