import React, { useEffect, useState } from 'react'
import useSound from 'use-sound'
import ReactMarkdown from 'react-markdown'
import jwt_decode from 'jwt-decode'
import question_right from '../../assets/gifs/question_right.png'
import question_wrong from '../../assets/gifs/question_wrong.png'
import success from '../../assets/sounds/success.wav'
import wrong from '../../assets/sounds/question_wrong.mp3'
import api from '../../services/api'
import './styles.css'

const QuestionMultiple = ({
	statement,
	question,
	alternative_1,
	alternative_2,
	alternative_3,
	difficulty,
	alternative_4,
	response_feedback,
	question_id,
	response,
	student_response
}) => {
	const [play] = useSound(success)
	const [playWrong] = useSound(wrong)
	const [questionId, setQuestionId] = useState(question_id)
	const [responseDisplay, setResponseDisplay] = useState(student_response)
	const [studentResponse, setStudentResponse] = useState('')
	const [questionWrongImage, setQuestionWrongImage] = useState(false)
	const [questionRightImage, setQuestionRightImage] = useState(false)
	const [hide, setHide] = useState()
	async function handleSubmit() {
		const token_data = jwt_decode(localStorage.getItem('@conig-Token'))

		await api
			.post(`/studentResponse`, {
				question_id,
				student_id: token_data.uid,
				studentResponse
			})
			.then(response => response.data)
			.then(result => {
				if (result === true) {
					play()
					setQuestionRightImage(true)
					setTimeout(() => {
						setQuestionRightImage(false)
					}, 1000)
				} else {
					setQuestionWrongImage(true)
					playWrong()
					setTimeout(() => {
						setQuestionWrongImage(false)
					}, 1000)
				}
			})

		setResponseDisplay({ response: true })
	}

	async function handleHide() {
		setHide(true)
	}
	useEffect(() => {
		if (responseDisplay) {
			setResponseDisplay(true)
		} else {
			setResponseDisplay(false)
		}
	}, [responseDisplay])
	return (
		<div
			className={`exercise__container ${responseDisplay ? '' : 'exercise__padding'} ${
				hide ? 'none' : 0
			}`}
		>
			<img
				src={question_right}
				className={`question-right ${
					questionRightImage ? 'question-right-animation__in' : 'question-right-animation__out'
				}`}
			/>
			<img
				src={question_wrong}
				className={`question-right ${
					questionWrongImage ? 'question-right-animation__in' : 'question-right-animation__out'
				}`}
			/>
			<div className={`exercise__modal  ${responseDisplay && studentResponse ? 'none' : ''} `}>
				<span className="exercise__enunciated">
					<ReactMarkdown>{statement}</ReactMarkdown>
					<br />
					<strong>{question}</strong>
					{console.log(student_response, alternative_3)}
				</span>
				{student_response ? (
					<form className="response">
						<div className="option_div">
							<input
								type="radio"
								name="response"
								onChange={e => setStudentResponse(e.target.value)}
								className="option blocked"
								value={alternative_1}
								checked={`${student_response === alternative_1 ? 'checked' : studentResponse}`}
								disabled={`${student_response === alternative_1 ? 'disabled' : studentResponse}`}
							/>
							<label className="form-check-label blocked" htmlFor="response">
								{alternative_1}
							</label>
						</div>
						<div className="option_div">
							<input
								type="radio"
								name="response blocked"
								onChange={e => setStudentResponse(e.target.value)}
								className="option"
								value={alternative_2}
								checked={`${student_response === alternative_2 ? 'checked' : studentResponse}`}
								disabled={`${student_response === alternative_2 ? 'disabled' : studentResponse}`}
							/>
							<label className="form-check-label blocked" htmlFor="response">
								{alternative_2}
							</label>
						</div>
						<div className="option_div">
							<input
								type="radio"
								name="response"
								onChange={e => setStudentResponse(e.target.value)}
								className="option blocked"
								value={alternative_3}
								checked={`${student_response === alternative_3 ? 'checked' : studentResponse}`}
								disabled={`${student_response === alternative_3 ? 'disabled' : studentResponse}`}
							/>
							<label className="form-check-label blocked" htmlFor="response">
								{alternative_3}
							</label>
						</div>
						<div className="option_div">
							<input
								type="radio"
								name="response "
								onChange={e => setStudentResponse(e.target.value)}
								className="option blocked"
								value={alternative_4}
								checked={`${student_response === alternative_4 ? 'checked' : studentResponse}`}
								disabled={`${student_response === alternative_4 ? 'disabled' : studentResponse}`}
							/>
							<label className="form-check-label blocked" htmlFor="response">
								{alternative_4}
							</label>
						</div>
					</form>
				) : (
					<form className="response">
						<div className="option_div">
							<input
								type="radio"
								name="response"
								onChange={e => setStudentResponse(e.target.value)}
								className="option"
								value={alternative_1}
							/>
							<label className="form-check-label" htmlFor="response">
								{alternative_1}
							</label>
						</div>
						<div className="option_div">
							<input
								type="radio"
								name="response"
								onChange={e => setStudentResponse(e.target.value)}
								className="option"
								value={alternative_2}
							/>
							<label className="form-check-label" htmlFor="response">
								{alternative_2}
							</label>
						</div>
						<div className="option_div">
							<input
								type="radio"
								name="response"
								onChange={e => setStudentResponse(e.target.value)}
								className="option"
								value={alternative_3}
							/>
							<label className="form-check-label" htmlFor="response">
								{alternative_3}
							</label>
						</div>
						<div className="option_div">
							<input
								type="radio"
								name="response"
								onChange={e => setStudentResponse(e.target.value)}
								className="option"
								value={alternative_4}
							/>
							<label className="form-check-label" htmlFor="response">
								{alternative_4}
							</label>
						</div>
						<button
							type="button"
							className={`response__btn ${responseDisplay ? 'disable__button' : ''}`}
							disabled={responseDisplay}
							onClick={handleSubmit}
						>
							Responder
						</button>
					</form>
				)}
			</div>
			<div
				className={`response__modal ${responseDisplay ? 'response__display' : ''} ${
					responseDisplay && studentResponse ? 'responsed' : ''
				}`}
			>
				A resposta é: <strong>{response}</strong>
				<br />
				<strong>Por quê?</strong>
				<br />
				{response_feedback}
				<div className="w-100 flex flex-wrap">
					<button type="button" className="hide_btn" onClick={handleHide}>
						Esconder
					</button>
					<div className="xp_gained">
						<p className="w-full">
							Nivel: {difficulty === 1 && 'Fácil'}
							{difficulty === 2 && 'Médio'}
							{difficulty === 3 && 'Dificil'}
						</p>
						<p className="w-full">
							XP ganho: {difficulty === 1 && (student_response === response ? '50' : '25')}
							{difficulty === 2 && (student_response === response ? '75' : '30')}
							{difficulty === 3 && (student_response === response ? '100' : '35')}
						</p>
					</div>
				</div>
			</div>
		</div>
	)
}

export default QuestionMultiple
