import React from 'react'
import './styles.css'
import medalIcon from '../../assets/icons/medal-icon.svg'
const ProfileCard = ({ handleClose, achievement, children }) => {
	return (
		<div className="profile-card__main">
			<div>
				<span className="profile-card__content">
					<img src={medalIcon} className="profile-card__medal" />
					{achievement}
				</span>
			</div>
		</div>
	)
}

export default ProfileCard
