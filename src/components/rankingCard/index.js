import React from 'react'
import './styles.css'

const RankingCard = ({ data, title }) => {
	let position = 0
	return (
		<div className="ranking__card">
			<p className="ranking__header">{title}</p>
			<table className="ranking__table">
				<thead>
					<th className="ranking__table-header">Posição</th>
					<th className="ranking__table-header">Nome</th>
					<th className="ranking__table-header">Experiencia</th>
				</thead>
				<tbody>
					{data.map(user => {
						// eslint-disable-next-line no-lone-blocks
						{
							position += 1
						}
						return (
							<tr className="ranking__table-row">
								<td className="ranking__table-column">
									<a href={`/profile/${user.student.username}`}>{position}º</a>
								</td>
								<td className="ranking__table-column">
									{' '}
									<a href={`/profile/${user.student.username}`}>{user.student.username}</a>
								</td>
								<td className="ranking__table-column total">
									{' '}
									<a href={`/profile/${user.student.username}`}>{user.total} XP</a>
								</td>
							</tr>
						)
					})}
				</tbody>
			</table>
		</div>
	)
}

export default RankingCard
