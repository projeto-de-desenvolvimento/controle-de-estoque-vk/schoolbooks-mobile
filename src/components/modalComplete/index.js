import React from 'react'
import { useHistory } from 'react-router'
// import './styles.css'

const ModalComplete = ({ handleClose, show, module_id, children }) => {
	const showHideClassName = show ? 'modal display-block' : 'modal display-none'
	let history = useHistory()
	async function handleFeedback() {
		history.push('/courses')
		// handleClose()
		// window.location.reload()
	}

	return (
		<div className={showHideClassName}>
			<div className="modal-main">
				<div className="modal-title">Parabens por completar o módulo.</div>
				<hr className="modal-hr"></hr>
				<div className="modal-content">
					Você conseguiu completar o módulo? Nossa! Você é realmente incrivel.
					<br></br>
					Você está preparado para seus próximos desafios!
					<br></br>
					Clique no botão abaixo para voltar a listagem de cursos
				</div>
				<button className="modal-button" onClick={handleFeedback}>
					Ok
				</button>
			</div>
		</div>
	)
}

export default ModalComplete
