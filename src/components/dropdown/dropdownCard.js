import React from 'react'
import { Link } from 'react-router-dom'
import './styles.css'

const liCls =
	'p-3 border mobile_dropdown_button bg-gray-100 w-44 text-gray-700 hover:text-white hover:bg-indigo-700 cursor-pointer'

const DropDownCard = ({ data = [], setOpen }) => (
	<div className="shadow mobile_dropdown h-auto w-44 absolute top_92 m-auto z-10">
		<ul className="text-left">
			{data.map((item, i) => (
				<Link to={item.link}>
					<li key={i} className={liCls} onClick={() => setOpen(false)}>
						{item.name}
					</li>
				</Link>
			))}
		</ul>
	</div>
)

export default DropDownCard
