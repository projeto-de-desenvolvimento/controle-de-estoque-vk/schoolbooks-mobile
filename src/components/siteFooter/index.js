import React from 'react'
import './styles.css'
function SiteFooter() {
	return (
		<footer class="w-full bg-gray-800 pt-2 mt-10 footer">
			<div class="m-auto text-gray-800 flex flex-wrap justify-left">
				<div class="p-5 w-full">
					<div class="mt-2 text-gray-600 text-center">
						© Copyright 2021-now. All Rights Reserved.
					</div>
				</div>
			</div>
		</footer>
	)
}

export default SiteFooter
