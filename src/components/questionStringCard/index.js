import React, { useEffect, useState } from 'react'
import useSound from 'use-sound'
import ReactMarkdown from 'react-markdown'
import jwt_decode from 'jwt-decode'
import question_right from '../../assets/gifs/question_right.png'
import question_wrong from '../../assets/gifs/question_wrong.png'
import success from '../../assets/sounds/success.wav'
import wrong from '../../assets/sounds/question_wrong.mp3'
import api from '../../services/api'
import './styles.css'

const QuestionString = ({
	statement,
	question,
	response_feedback,
	question_id,
	response,
	difficulty,
	student_response
}) => {
	const [playWrong] = useSound(wrong)
	const [play] = useSound(success)
	const [questionWrongImage, setQuestionWrongImage] = useState(false)
	const [questionRightImage, setQuestionRightImage] = useState(false)
	const [questionId, setQuestionId] = useState(question_id)
	const [responseDisplay, setResponseDisplay] = useState(student_response)
	const [studentResponse, setStudentResponse] = useState()
	const [hide, setHide] = useState()
	async function handleSubmit() {
		const token_data = jwt_decode(localStorage.getItem('@conig-Token'))

		await api
			.post(`/studentResponse`, {
				question_id,
				student_id: token_data.uid,
				studentResponse
			})
			.then(response => response.data)
			.then(result => {
				if (result === true) {
					play()
					setQuestionRightImage(true)
					setTimeout(() => {
						setQuestionRightImage(false)
					}, 1000)
				} else {
					setQuestionWrongImage(true)
					playWrong()
					setTimeout(() => {
						setQuestionWrongImage(false)
					}, 1000)
				}
			})

		setResponseDisplay({ response: true })
	}

	async function handleHide() {
		setHide(true)
	}

	useEffect(() => {
		if (responseDisplay) {
			setResponseDisplay(true)
		} else {
			setResponseDisplay(false)
		}
	}, [responseDisplay])
	return (
		<div
			className={`exercise__container ${responseDisplay ? '' : 'exercise__padding'} ${
				hide ? 'none' : 0
			}`}
		>
			<img
				src={question_right}
				className={`question-right ${
					questionRightImage ? 'question-right-animation__in' : 'question-right-animation__out'
				}`}
			/>
			<img
				src={question_wrong}
				className={`question-right ${
					questionWrongImage ? 'question-right-animation__in' : 'question-right-animation__out'
				}`}
			/>
			<div className={`exercise__modal ${responseDisplay && studentResponse ? 'none' : ''}`}>
				<span className="exercise__enunciated">
					<ReactMarkdown>{statement}</ReactMarkdown>
					<br />
					<strong>{question}</strong>
				</span>
				{student_response ? (
					<form className="response">
						<label htmlFor="student_response">Resposta:</label>
						<input
							className=" text-md py-1 border-b rounded-sm border-gray-300 focus:outline-none focus:border-indigo-500"
							type="text"
							name="student_response"
							value={student_response}
							onChange={e => setStudentResponse(e.target.value)}
							disabled={`${student_response ? 'disabled' : studentResponse}`}
						/>
						<button
							type="button"
							className={`response__btn ${responseDisplay ? 'disable__button' : ''}`}
							disabled={responseDisplay}
							onClick={handleSubmit}
						>
							Responder
						</button>
					</form>
				) : (
					<form className="response">
						<label htmlFor="student_response">Resposta:</label>
						<input
							className=" text-md py-1 border-b rounded-sm border-gray-300 focus:outline-none focus:border-indigo-500"
							type="text"
							name="student_response"
							onChange={e => setStudentResponse(e.target.value)}
						/>
						<button
							type="button"
							className={`response__btn ${responseDisplay ? 'disable__button' : ''}`}
							disabled={responseDisplay}
							onClick={handleSubmit}
						>
							Responder
						</button>
					</form>
				)}
			</div>

			<div
				className={`response__modal ${responseDisplay ? 'response__display' : ''} ${
					responseDisplay && studentResponse ? 'responsed' : ''
				}`}
			>
				A resposta é: <strong>{response}</strong>
				<br />
				<strong>Por quê?</strong>
				<br />
				{response_feedback}
				<div className="w-100">
					<button type="button" className="hide_btn" onClick={handleHide}>
						Esconder
					</button>
					<div className="xp_gained">
						<p className="w-full">
							Nivel: {difficulty === 1 && 'Fácil'}
							{difficulty === 2 && 'Médio'}
							{difficulty === 3 && 'Dificil'}
						</p>
						<p className="w-full">
							XP ganho: {difficulty === 1 && (student_response === response ? '50' : '25')}
							{difficulty === 2 && (student_response === response ? '75' : '30')}
							{difficulty === 3 && (student_response === response ? '100' : '35')}
						</p>
					</div>
				</div>
			</div>
		</div>
	)
}

export default QuestionString
