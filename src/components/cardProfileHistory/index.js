import Markdown from 'markdown-to-jsx'
import React, { useEffect, useState } from 'react'
// import './styles.css'

const CardProfileHistory = ({ handleClose, exercise, tutorial, children }) => {
	return (
		<div className="profile-card__main">
			<div>
				<span className="profile-card__content">
					<svg
						class="w-6 h-6 mr-2"
						fill="none"
						stroke="currentColor"
						viewBox="0 0 24 24"
						xmlns="http://www.w3.org/2000/svg"
					>
						<path
							stroke-linecap="round"
							stroke-linejoin="round"
							stroke-width="2"
							d="M10 20l4-16m4 4l4 4-4 4M6 16l-4-4 4-4"
						></path>
					</svg>
					Parabéns por completar o {exercise ? 'EXERCICIO' : 'TUTORIAL'}{' '}
					{exercise ? exercise : tutorial}
				</span>
			</div>
		</div>
	)
}

export default CardProfileHistory
