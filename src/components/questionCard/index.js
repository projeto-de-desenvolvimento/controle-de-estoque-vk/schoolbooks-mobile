import React, { useEffect, useState } from 'react'
import useSound from 'use-sound'
import useSWR, { mutate } from 'swr'
import ReactMarkdown from 'react-markdown'
import jwt_decode from 'jwt-decode'
import question_right from '../../assets/gifs/question_right.png'
import question_wrong from '../../assets/gifs/question_wrong.png'
import success from '../../assets/sounds/success.wav'
import wrong from '../../assets/sounds/question_wrong.mp3'
import api from '../../services/api'
import './styles.css'

const QuestionCard = ({
	statement,
	question,
	response,
	response_feedback,
	question_id,
	difficulty,
	student_response
}) => {
	const [questionResponse, setQuestionResponse] = useState(response)
	const [playWrong] = useSound(wrong, { volume: 0.25 })
	const [play] = useSound(success, { volume: 0.25 })
	const [questionId, setQuestionId] = useState(question_id)
	const [questionWrongImage, setQuestionWrongImage] = useState(false)
	const [responseDisplay, setResponseDisplay] = useState(!!student_response)
	const [questionRightImage, setQuestionRightImage] = useState(false)
	const [studentResponse, setStudentResponse] = useState('')
	const [hide, setHide] = useState(false)
	async function handleSubmit() {
		const token_data = jwt_decode(localStorage.getItem('@conig-Token'))

		await api
			.post(`/studentResponse`, {
				question_id,
				student_id: token_data.uid,
				questionResponse,
				studentResponse
			})
			.then(response => response.data)
			.then(result => {
				if (result === true) {
					play()
					setQuestionRightImage(true)
					setTimeout(() => {
						setQuestionRightImage(false)
					}, 1000)
				} else {
					setQuestionWrongImage(true)
					playWrong()
					setTimeout(() => {
						setQuestionWrongImage(false)
					}, 1000)
				}
			})

		setResponseDisplay({ response: true })
	}

	async function handleHide() {
		setHide(true)
	}
	useEffect(() => {
		if (responseDisplay) {
			setResponseDisplay(true)
		} else {
			setResponseDisplay(false)
		}
	}, [responseDisplay])
	return (
		<div
			className={`exercise__container ${responseDisplay ? '' : 'exercise__padding'} ${
				hide ? 'none' : 0
			}`}
		>
			<img
				src={question_right}
				className={`question-right ${
					questionRightImage ? 'question-right-animation__in' : 'question-right-animation__out'
				}`}
			/>
			<img
				src={question_wrong}
				className={`question-right ${
					questionWrongImage ? 'question-right-animation__in' : 'question-right-animation__out'
				}`}
			/>
			<div className={`exercise__modal ${responseDisplay && studentResponse ? 'none' : ''}`}>
				<span className="exercise__enunciated">
					<ReactMarkdown>{statement}</ReactMarkdown>
					<br />
					<strong>{question}</strong>
				</span>

				{student_response ? (
					<form className="response">
						<div className="option_div">
							<input
								type="radio"
								name="response"
								onChange={e => setStudentResponse(e.target.value)}
								className="option blocked"
								value="V"
								checked={`${student_response === 'F' ? 'checked' : studentResponse}`}
								disabled={`${student_response === 'F' ? 'disabled' : studentResponse}`}
							/>
							<label className="form-check-label blocked" htmlFor="response">
								Falso
							</label>
						</div>
						<div className="option_div">
							<input
								type="radio"
								name="response"
								onChange={e => setStudentResponse(e.target.value)}
								className="option blocked"
								value="F"
								checked={`${student_response === 'V' ? 'checked' : studentResponse}`}
								disabled={`${student_response === 'V' ? 'disabled' : studentResponse}`}
							/>
							<label className="form-check-label blocked" htmlFor="response">
								Verdadeiro
							</label>
						</div>
					</form>
				) : (
					<form className="response">
						<div className="option_div">
							<input
								type="radio"
								name="response"
								onChange={e => setStudentResponse(e.target.value)}
								className="option"
								value="F"
							/>
							<label className="form-check-label" htmlFor="response">
								Falso
							</label>
						</div>
						<div className="option_div">
							<input
								type="radio"
								name="response"
								onChange={e => setStudentResponse(e.target.value)}
								className="option"
								value="V"
							/>
							<label className="form-check-label" htmlFor="response">
								Verdadeiro
							</label>
						</div>
						<button type="button" className="response__btn" onClick={handleSubmit}>
							Responder
						</button>
					</form>
				)}
			</div>

			<div
				className={`response__modal ${responseDisplay ? 'response__display' : ''} ${
					responseDisplay && studentResponse ? 'responsed' : ''
				}`}
			>
				A resposta é: <strong>{response === 'V' ? 'Verdadeira' : 'Falsa'}</strong>
				<br />
				<strong>Por quê?</strong>
				<br />
				{response_feedback}
				<div className="w-100 flex flex-wrap">
					<button
						type="button"
						className={`hide_btn ${student_response ? 'none' : ''}`}
						onClick={handleHide}
					>
						Esconder
					</button>
					<div className="xp_gained">
						<p className="w-full">
							Nivel: {difficulty === 1 && 'Fácil'}
							{difficulty === 2 && 'Médio'}
							{difficulty === 3 && 'Dificil'}
						</p>
						<p className="w-full">
							XP ganho: {difficulty === 1 && (student_response === response ? '50' : '25')}
							{difficulty === 2 && (student_response === response ? '75' : '30')}
							{difficulty === 3 && (student_response === response ? '100' : '35')}
						</p>
					</div>
				</div>
			</div>
		</div>
	)
}

export default QuestionCard
