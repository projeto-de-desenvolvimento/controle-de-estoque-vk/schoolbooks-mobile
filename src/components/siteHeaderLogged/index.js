import React, { useEffect, useState } from 'react'
import jwt_decode from 'jwt-decode'
import { Link, Redirect, useHistory } from 'react-router-dom'
import useSound from 'use-sound'
import logo_conig from '../../assets/images/logo.png'
import DropDownCard from '../dropdown/dropdownCard'
import LevelUp from '../../assets/sounds/levelup.wav'
import Button from '../dropdown/button'
import api, { useFetch } from '../../services/api'
import NotificationCard from '../notificationCard'

function SiteHeaderLogged(props) {
	const [isLogged, setLogged] = useState(false)
	const [level, setLevel] = useState('')
	const [play] = useSound(LevelUp, { volume: 0.25 })
	const [redirect, setRedirect] = useState(false)
	const history = useHistory()

	const { data: student } = useFetch('/profile')

	const profileLinks = [
		{ name: 'Perfil', link: '/profile' },
		{ name: 'Sair', link: '/logout' }
	]

	const [open, setOpen] = React.useState(false)
	const [active, setActive] = React.useState(false)
	const [icon, setIcon] = React.useState(
		<svg
			className="w-6 h-6 mobile_icon float-right md:ml-2"
			fill="none"
			stroke="currentColor"
			viewBox="0 0 24 24"
			xmlns="http://www.w3.org/2000/svg"
		>
			<path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M19 9l-7 7-7-7" />
		</svg>
	)
	const drop = React.useRef(null)

	function handleClick(e) {
		if (!e.target.closest(`.${drop.current.className}`) && open) {
			setOpen(false)
		}

		if (active) {
			setActive(false)
			setIcon(
				<svg
					className="w-6 h-6 float-right md:ml-2"
					fill="none"
					stroke="currentColor"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
				>
					<path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M19 9l-7 7-7-7" />
				</svg>
			)
		} else {
			setActive(true)
			setIcon(
				<svg
					className="w-6 h-6 float-right md:ml-2"
					fill="none"
					stroke="currentColor"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
				>
					<path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 15l7-7 7 7" />
				</svg>
			)
		}
	}
	React.useEffect(() => {
		document.addEventListener('click', handleClick)
		return () => {
			document.removeEventListener('click', handleClick)
		}
	})
	function logOut() {
		if (localStorage.removeItem('@conig-Token')) setLogged(false)
		setRedirect(true)
	}

	return (
		<div className="">
			<nav
				className={`w-full h-18 z-30 top-10 py-1 shadow-lg border-b-4 border-yellow-500 navigation
				`}
			>
				{student && student.level_up && play()}
				<div className="flex flex-col w-full px-4 md:flex-row md:px-6 lg:px-8">
					<Link to="/initial-page" className="md:mt-o md:pt-0 md:mx-0 place-self-center md:mr-auto">
						<img src={logo_conig} alt="Logo Conig" className="w-25 logo" />
					</Link>
					<ul className="flex flex-col mt-4 -mx-4 pt-4 border-t md:flex-row md:items-center md:mx-0 md:ml-auto md:mt-0 md:pt-0 md:border-0">
						<li>
							<Link
								to="/courses"
								className="block mobile px-6 py-2 text-xs font-medium leading-6 text-center text-white uppercase transition bg-yellow-600 rounded-full shadow ripple hover:shadow-lg hover:bg-yellow-500 focus:outline-none float-right m-6"
							>
								Módulos
								<svg
									className="w-6 h-6 float-right md:ml-2"
									fill="none"
									stroke="currentColor"
									viewBox="0 0 24 24"
									xmlns="http://www.w3.org/2000/svg"
								>
									<path
										strokeLinecap="round"
										strokeLinejoin="round"
										strokeWidth="2"
										d="M12 6.253v13m0-13C10.832 5.477 9.246 5 7.5 5S4.168 5.477 3 6.253v13C4.168 18.477 5.754 18 7.5 18s3.332.477 4.5 1.253m0-13C13.168 5.477 14.754 5 16.5 5c1.747 0 3.332.477 4.5 1.253v13C19.832 18.477 18.247 18 16.5 18c-1.746 0-3.332.477-4.5 1.253"
									/>
								</svg>
							</Link>
						</li>
						<li>
							<Link
								to="/ranking"
								className="block mobile px-6 py-2 text-xs font-medium leading-6 text-center text-white uppercase transition bg-yellow-600 rounded-full shadow ripple hover:shadow-lg hover:bg-yellow-500 focus:outline-none float-right m-6"
							>
								Ranking
							</Link>
						</li>
						<li />
						<li>
							{student && (
								<div ref={drop} className="dropdown">
									<Button
										onClick={() => setOpen(open => !open)}
										name={student.username}
										image={student.profile_pic}
										progress={student.progress}
										level={student.level}
										icon={icon}
									/>
									{open && <DropDownCard data={profileLinks} setOpen={setOpen} />}
								</div>
							)}
						</li>
					</ul>
				</div>
			</nav>
			{student && (
				<NotificationCard
					show={student.level_up}
					title="Novo nivel alcançado"
					content={`Parabéns por alcançar o nivel ${student.level}`}
				/>
			)}
		</div>
	)
}

export default SiteHeaderLogged
