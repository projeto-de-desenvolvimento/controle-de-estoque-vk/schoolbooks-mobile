import React from 'react'
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom'
import Landing from './pages/landing'
import { isAuthenticated } from './services/auth'

import Register from './pages/register'
import InitialPage from './pages/initial-page'
import Login from './pages/login'
import Logout from './pages/logout'
import Confirmation from './pages/confirmation-page'
import Courses from './pages/courses'
import Course_Page from './pages/course-page'
import Course from './pages/course-overview'
import Exercise from './pages/exercise-page'
import Ranking from './pages/ranking'
import Tutorial from './pages/tutorial-page'
import Profile from './pages/profile'
import Forgot from './pages/forgot-password'
import Reset from './pages/reset-password'
import Community_Profile from './pages/community-profile'

const PrivateRoute = ({ component: Component, ...rest }) => (
	<Route
		{...rest}
		render={props =>
			isAuthenticated() ? (
				<Component {...props} />
			) : (
				<Redirect to={{ pathname: '/', state: { from: props.location } }} />
			)
		}
	/>
)

function Routes() {
	return (
		<BrowserRouter>
			<Switch>
				<Route path="/" exact component={Landing} />
				<Route path="/signup" exact component={Register} />
				<Route path="/signin" exact component={Login} />
				<Route path="/confirmation/:confirmation/:id" exact component={Confirmation} />
				<Route path="/confirmation//:id" exact component={Confirmation} />
				<Route path="/courses" exact component={Courses} />
				<Route path="/courses/:id" exact component={Course_Page} />
				<Route path="/ranking" exact component={Ranking} />
				<Route path="/forgot" exact component={Forgot} />
				<Route path="/reset/:token" exact component={Reset} />
				<Route path="/profile/:username" exact component={Community_Profile} />
				<PrivateRoute path="/logout" exact component={Logout} />
				<PrivateRoute path="/initial-page" exact component={InitialPage} />
				<PrivateRoute path="/module/:id" exact component={Course} />
				<PrivateRoute path="/module/:module_id/tutorial/:id" exact component={Tutorial} />
				<PrivateRoute path="/module/:module_id/exercise/:id" exact component={Exercise} />
				<PrivateRoute path="/profile" exact component={Profile} />
			</Switch>
		</BrowserRouter>
	)
}

export default Routes
