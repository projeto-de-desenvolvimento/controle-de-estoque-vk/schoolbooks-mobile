import Markdown from 'markdown-to-jsx'
import React, { useEffect, useState } from 'react'
import ReactMarkdown from 'react-markdown'
import ReactPlayer from 'react-player'
import { useParams } from 'react-router'
import { Redirect } from 'react-router-dom'
import rehypeRaw from 'rehype-raw'
import remarkGfm from 'remark-gfm'
import Modal from '../../components/feedback-modal'
import ModalComplete from '../../components/modalComplete'
import SideNav from '../../components/sideNav'
import SiteHeader from '../../components/siteHeader'
import api from '../../services/api'
import './styles.css'

function Tutorial() {
	const [module, setModule] = useState({})
	const [nextLink, setNextLink] = useState('')
	const [tutorial, setTutorial] = useState({})
	const [content, setContent] = useState('Vazio')
	const [links, setLinks] = useState([])
	const [redirect, setRedirect] = useState(false)
	const [showModal, setShowModal] = useState(false)
	const [showModalFeedback, setShowModalFeedback] = useState(false)
	const { id, module_id } = useParams()
	console.log(`${id}`)
	useEffect(() => {
		async function getTutorial() {
			try {
				const { data } = await api.get(`/modules/${module_id}`)
				const tutorial = await api.get(`/tutorials/${id}`)
				setModule(data.module)
				setTutorial(tutorial.data)
				setLinks(data.module.classes)
			} catch (err) {
				console.log(err)
			}
		}
		getTutorial()
	}, [id])

	setTimeout(() => {
		setContent(tutorial.content)
	}, 150)

	async function completeCourse() {
		await api.post('/studentTutorial', {
			tutorial_id: id
		})
		setShowModal(true)
	}
	function openFeedback() {
		setShowModalFeedback(true)
	}
	async function openModal() {
		await api.post('/studentTutorial', {
			tutorial_id: id
		})
		setNextLink(`/module/${module_id}${tutorial.next_tutorial}`)
		setRedirect(true)
	}
	function closeModal() {
		setShowModalFeedback(false)
	}

	return (
		<div>
			{redirect && <Redirect to={nextLink} />}
			<SiteHeader />
			{tutorial.next_tutorial !== 'last' && (
				<Modal
					show={showModalFeedback}
					handleClose={() => closeModal()}
					next_tutorial={nextLink}
					tutorial_id={id}
				/>
			)}
			{tutorial.next_tutorial === 'last' && (
				<ModalComplete show={showModal} module_id={module_id} />
			)}
			<main className="main-container">
				<SideNav
					data={links}
					module_name={module.name}
					module_id={module_id}
					tutorial_id={id}
					is_exercise={false}
				/>
				<section className="content">
					{!tutorial.link && (
						<img alt="module_image" src={module.module_image_link} className="content__image" />
					)}
					{tutorial.link && <ReactPlayer url={tutorial.link} className="content__image" controls />}
					<h1 className="content__title">{tutorial.title}</h1>
					<h1 className="content__subtitle">
						<ReactMarkdown>{tutorial.description}</ReactMarkdown>
					</h1>
					<span className="content__overview" id="content__tutorial">
						<Markdown options={{ forceBlock: true }}>
							{tutorial.content ? tutorial.content : '**VAZIO**'}
						</Markdown>
					</span>
					<span className="content__ready">
						Já terminou a aula? QUE INCRIVEL! Clique no botão para completar esta aula!
					</span>
					{tutorial.next_tutorial !== 'last' && (
						<>
							<button
								type="button"
								onClick={openModal}
								className={`content__ready--button ${tutorial.is_completed && 'blocked'}`}
								disabled={tutorial.is_completed && true}
							>
								Completar tutorial
							</button>
							<div className="w-100">
								<button type="button" onClick={openFeedback} className="content__ready--feedback">
									De o seu Feedback
								</button>
							</div>
						</>
					)}
					{tutorial.next_tutorial === 'last' && (
						<>
							<button type="button" onClick={completeCourse} className="content__ready--button">
								Completar módulo
							</button>
							<div className="w-100">
								<button type="button" onClick={openFeedback} className="content__ready--feedback">
									De o seu Feedback
								</button>
							</div>
						</>
					)}
				</section>
			</main>
		</div>
	)
}

export default Tutorial
