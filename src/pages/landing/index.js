import React, { Component, useEffect, useState } from 'react'
import './styles.css'

import { Redirect } from 'react-router'
import { login } from '../../services/auth'
import api from '../../services/api'
import SiteHeader from '../../components/siteHeader'
import SiteFooter from '../../components/siteFooter'
import hero from '../../assets/images/hero-landing.jpg'
import Modal from '../../components/modal'
import Loading from '../../components/loading'

function Landing() {
	const [courses, setCourses] = useState([])
	const [email, setEmail] = useState('')
	const [rediret, setRedirect] = useState(false)
	const [password, setPassword] = useState('')
	const [username, setUsername] = useState('')
	const [errorMessage, setErrorMessage] = useState('')
	const [loading, setLoading] = useState(false)
	const [name, setName] = useState('')
	const [show, setShow] = useState('')

	// useEffect(() => {
	// 	async function getCourses() {
	// 		try {
	// 			const { data } = await api.get('/topcourses')
	// 			setCourses(data)
	// 		} catch (error) {
	// 			alert(error + ' Ocorreu um erro ao buscar os items')
	// 		}
	// 	}
	// 	getCourses()
	// }, [])

	async function signUp(event) {
		event.preventDefault()
		setLoading(true)
		const response = await api
			.post('/students', {
				complete_name: name,
				username,
				email,
				password
			})
			.then(function(response) {
				if (response.data) {
					setShow(true)
				}
				setLoading(false)
			})
			.catch(function(error) {
				setLoading(false)
				setErrorMessage(error.response.data[0].message)
				document.getElementById('error').className = 'text-red-500'
			})
		try {
			console.log(response.data)
			if (response.data) {
				setShow(true)
			}
		} catch (err) {
			console.log(err)
		}
	}

	async function closeModal(event) {
		event.preventDefault()
		setShow(false)
		const response = await api.post('/login', {
			email,
			password
		})
		try {
			login(response.data.token)
			localStorage.setItem('@conig-Token', response.data.token)
			setRedirect(true)
		} catch (err) {
			console.log('Erro')
		}
	}
	return (
		<div>
			{rediret && <Redirect to="/initial-page" />}
			<SiteHeader />
			<Modal show={show} handleClose={e => closeModal(e)} />
			<div className="landing-main-container z-0 opacity-90 w-full absolute ">
				<div className=" w-full relative sm:items-center gradient">
					<div className="w-2/3 text-white flex md:ml-32 flex-col">
						<h1 className="sm:text-5xl text-xl font-weight-bolder sm:mb-6">
							O melhor local para iniciar no mundo da programação!
						</h1>

						<h2 className="sm:text-2xl text-base mb-2">Seus estudos começam aqui.</h2>
						<p className="text-xs sm:text-base">
							Cursos introdutórios gratuitos para que possa entrar nesse mundo de cabeça.
						</p>
					</div>
					<div>
						<div className="bg-white form-newsletter ">
							<h2
								className="text-center text-4xl text-indigo-900 font-display font-semibold lg:text-left xl:text-5xl
                    xl:text-bold"
							>
								Cadastre gratuitamente
							</h2>
							<form className="mt-12" onSubmit={e => signUp(e)}>
								<p id="error" className="hidden text-red-500">
									{errorMessage}
								</p>
								<div>
									<div className="text-sm font-bold text-gray-700 tracking-wide">Nome Completo</div>
									<input
										className="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500"
										type="text"
										value={name}
										name="complete_name"
										placeholder="João da Silva"
										onChange={e => setName(e.target.value)}
									/>
									<div className="text-sm font-bold text-gray-700 tracking-wide">
										Nome de usuário
									</div>
									<input
										className="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500"
										type="text"
										value={username}
										name="nome de usuario"
										maxLength="12"
										placeholder="Jsilva"
										onChange={e => setUsername(e.target.value)}
									/>
									<span className="instructions">
										Não poderá conter espaços ou acentuação gráfica.
									</span>
									<div className="text-sm font-bold text-gray-700 tracking-wide">
										Endereço de E-mail
									</div>
									<input
										className="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500"
										type="text"
										value={email}
										name="email"
										placeholder="email@gmail.com"
										onChange={e => setEmail(e.target.value)}
									/>
									<div className="text-sm font-bold text-gray-700 tracking-wide">Senha</div>
									<input
										className="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500"
										type="password"
										value={password}
										name="senha"
										placeholder="*******"
										onChange={e => setPassword(e.target.value)}
									/>
									<span className="instructions">
										Entre 6 a 10 caracteres e pelo menos 1 número.
									</span>
									<div className="mt-8">
										<button
											className="bg-indigo-500 text-gray-100 p-4 w-full rounded-full tracking-wide
                                font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-indigo-600
                                shadow-lg"
											type="submit"
										>
											<span className={loading ? 'hidden' : 0}>
												Cadastrar-se agora
												<svg
													className="w-6 h-6 float-right"
													fill="none"
													stroke="currentColor"
													viewBox="0 0 24 24"
													xmlns="http://www.w3.org/2000/svg"
												>
													<path
														strokeLinecap="round"
														strokeLinejoin="round"
														strokeWidth="2"
														d="M19 20H5a2 2 0 01-2-2V6a2 2 0 012-2h10a2 2 0 012 2v1m2 13a2 2 0 01-2-2V7m2 13a2 2 0 002-2V9a2 2 0 00-2-2h-2m-4-3H9M7 16h6M7 8h6v4H7V8z"
													/>
												</svg>
											</span>
											<Loading loading={loading} />
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div className="bg-white w-full content__padding">
					<div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
						<div className="lg:text-center">
							<h2 className="text-base text-indigo-600 font-semibold tracking-wide uppercase">
								Conig - Plataforma de Aprendizagem
							</h2>
							<p className="mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">
								O melhor jeito de iniciar no mundo da programação
							</p>
							<p className="mt-4 max-w-2xl text-xl text-gray-500 lg:mx-auto">
								Perfeito para você que esta iniciando no mundo da programação hoje. E até mesmo para
								quem quer aprimorar suas habilidades.
							</p>
						</div>
						<div className="mt-10">
							<dl className="space-y-10 md:space-y-0 md:grid md:grid-cols-2 md:gap-x-8 md:gap-y-10">
								<div className="flex">
									<div className="flex-shrink-0">
										<div className="flex items-center justify-center h-12 w-12 rounded-md bg-indigo-500 text-white">
											<svg
												className="w-6 h-6"
												fill="none"
												stroke="currentColor"
												viewBox="0 0 24 24"
												xmlns="http://www.w3.org/2000/svg"
											>
												<path
													strokeLinecap="round"
													strokeLinejoin="round"
													strokeWidth="2"
													d="M12 6.253v13m0-13C10.832 5.477 9.246 5 7.5 5S4.168 5.477 3 6.253v13C4.168 18.477 5.754 18 7.5 18s3.332.477 4.5 1.253m0-13C13.168 5.477 14.754 5 16.5 5c1.747 0 3.332.477 4.5 1.253v13C19.832 18.477 18.247 18 16.5 18c-1.746 0-3.332.477-4.5 1.253"
												/>
											</svg>
										</div>
									</div>
									<div className="ml-4">
										<dt className="text-lg leading-6 font-medium text-gray-900">
											Cursos Gratuitos
										</dt>
										<dd className="mt-2 text-base text-gray-500">
											Desfrute de cursos introdutórios gratuitos com uma maneira descontraída de
											aprender. Logo logo você estará aprendendo conceitos de programação.
										</dd>
									</div>
								</div>

								<div className="flex">
									<div className="flex-shrink-0">
										<div className="flex items-center justify-center h-12 w-12 rounded-md bg-indigo-500 text-white">
											<svg
												className="h-6 w-6"
												xmlns="http://www.w3.org/2000/svg"
												fill="none"
												viewBox="0 0 24 24"
												stroke="currentColor"
												aria-hidden="true"
											>
												<path
													strokeLinecap="round"
													strokeLinejoin="round"
													strokeWidth="2"
													d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9"
												/>
											</svg>
										</div>
									</div>
									<div className="ml-4">
										<dt className="text-lg leading-6 font-medium text-gray-900">Aulas Online</dt>
										<dd className="mt-2 text-base text-gray-500">
											Na Conig as aulas são disponibilizadas de maneira totalmente online, com
											acesso vitalicio.
										</dd>
									</div>
								</div>

								<div className="flex">
									<div className="flex-shrink-0">
										<div className="flex items-center justify-center h-12 w-12 rounded-md bg-indigo-500 text-white">
											<svg
												className="w-6 h-6"
												fill="none"
												stroke="currentColor"
												viewBox="0 0 24 24"
												xmlns="http://www.w3.org/2000/svg"
											>
												<path
													strokeLinecap="round"
													strokeLinejoin="round"
													strokeWidth="2"
													d="M8 12h.01M12 12h.01M16 12h.01M21 12c0 4.418-4.03 8-9 8a9.863 9.863 0 01-4.255-.949L3 20l1.395-3.72C3.512 15.042 3 13.574 3 12c0-4.418 4.03-8 9-8s9 3.582 9 8z"
												/>
											</svg>
										</div>
									</div>
									<div className="ml-4">
										<dt className="text-lg leading-6 font-medium text-gray-900">
											Dúvidas? Contate-nos
										</dt>
										<dd className="mt-2 text-base text-gray-500">
											Na Conig é possivel retirar dúvidas diretamente conosco, mande um email para:
											conigplat@gmail.com.
										</dd>
									</div>
								</div>

								<div className="flex">
									<div className="flex-shrink-0">
										<div className="flex items-center justify-center h-12 w-12 rounded-md bg-indigo-500 text-white">
											<svg
												className="h-6 w-6"
												xmlns="http://www.w3.org/2000/svg"
												fill="none"
												viewBox="0 0 24 24"
												stroke="currentColor"
												aria-hidden="true"
											>
												<path
													strokeLinecap="round"
													strokeLinejoin="round"
													strokeWidth="2"
													d="M7 8h10M7 12h4m1 8l-4-4H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-3l-4 4z"
												/>
											</svg>
										</div>
									</div>
									<div className="ml-4">
										<dt className="text-lg leading-6 font-medium text-gray-900">
											Cursos básicos/intermediários/avançados
										</dt>
										<dd className="mt-2 text-base text-gray-500">
											Na Conig também disponibilizaremos cursos para quem já esta no mundo da
											programação, sendo estes de níveis intermediários até avançados.
										</dd>
									</div>
								</div>
							</dl>
						</div>
					</div>
				</div>
				<hr className="w-full md:ml-auto h-0.5 md:mr-auto bg-indigo-500" />
				<div className="grid md:grid-cols-3 gap-8 m-5 max-w-6xl m-auto">
					{courses.map(course => (
						<div className="flex flex-col md:ml-auto w-96 shadow-2xl md:mt-10 gap-2 m-5 max-w-5xl m-auto">
							<div className="bg-white rounded-2xl">
								<img
									src={course.course_image}
									alt=""
									className="w-full object-fill h-48 sm:h-56 object-cover rounded-md"
								/>

								<div className="px-10 py-6 mb-10 text-center">
									<div className="text-2xl font-bold text-yellow-500 mb-4">
										{course.course_name}
									</div>
									<span className="text-sm">{course.description}</span>
									<br />
									<a
										href="/teacher"
										className="mr-1 inline-flex items-center px-4 py-2 border border-gray-300 text-sm leading-6 font-medium rounded-md text-gray-900 bg-white gap-2 transition ease-in-out duration-150 card-button--teacher"
									>
										<svg
											className="w-6 h-6 card-icon--teacher"
											fill="none"
											stroke="#5707ec"
											viewBox="0 0 24 24"
											xmlns="http://www.w3.org/2000/svg"
										>
											<path d="M12 14l9-5-9-5-9 5 9 5z" />
											<path d="M12 14l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14z" />
											<path
												strokeLinecap="round"
												strokeLinejoin="round"
												strokeWidth="2"
												d="M12 14l9-5-9-5-9 5 9 5zm0 0l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14zm-4 6v-7.5l4-2.222"
											/>
										</svg>
										{course.teacher.username}
									</a>
								</div>

								<a
									href={`/course/${course.id}`}
									className="block px-6 py-2 text-xs font-medium leading-6 text-center text-white uppercase transition bg-yellow-600 rounded-full shadow ripple hover:shadow-lg hover:bg-yellow-500 focus:outline-none cursor-pointer place-self-center m-6"
								>
									Veja o curso
								</a>
							</div>
						</div>
					))}
				</div>
				<SiteFooter />
			</div>
		</div>
	)
}

export default Landing
