import React, { useEffect, useState } from 'react'
import { Redirect, useHistory } from 'react-router-dom'
import SiteHeader from '../../components/siteHeader'

function Logout() {
	const [isLogged, setLogged] = useState(false)
	let history = useHistory()
	useEffect(() => {
		if (localStorage.getItem('@conig-Token')) {
			setLogged(true)
		}
	}, [])

	if (localStorage.removeItem('@conig-Token')) setLogged(false)

	return !localStorage.getItem('@conig-Token') && <Redirect to={'/signin'}></Redirect>
}

export default Logout
