import React, { useEffect } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import SiteHeader from "../../components/siteHeader";
import api from "../../services/api";

function Confirmation(){
  const {id , confirmation} = useParams();

  useEffect(()=>{
    async function postConfirmation() {
      try{
        const {data} = await api.put("/user/"+id, {
          confirmation_token: confirmation
        })
      }catch(err){
        console.log(err)
      }
    }
    postConfirmation()
  })

    return(
      <div>
      <SiteHeader></SiteHeader>
      <div className="flex flex-col md:ml-auto w-2xl shadow-2xl md:mt-36 gap-2 m-5 max-w-5xl m-auto">
        <div className="bg-white rounded-2xl">

          <div className="px-10 py-6 mb-10 text-center">
            <div className="text-2xl font-bold text-yellow-500 mb-4">Email Confirmado</div>
            <span className="text-sm">
              Bem vindo a Conig Plataforma de aprendizagem!
              Clique no botão abaixo e comece a procurar o curso que deseja encontrar.
            </span>
          <a href={"/courses"} className="block px-6 py-2 text-xs w-36 mx-auto font-medium leading-6 text-center text-white uppercase transition bg-yellow-600 rounded-full shadow ripple hover:shadow-lg hover:bg-yellow-500 focus:outline-none cursor-pointer place-self-center m-6">Cursos</a>
        </div>
          </div>
          </div>
      </div>
  )
}

export default Confirmation;