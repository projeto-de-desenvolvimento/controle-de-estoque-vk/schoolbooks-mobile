import React, { useEffect, useRef, useState } from 'react'
import jwt_decode from 'jwt-decode'
import { useParams } from 'react-router'
import edit from '../../assets/icons/pencil.svg'
import SiteHeader from '../../components/siteHeader'
import api from '../../services/api'
import './styles.css'
import ProfileCard from '../../components/profileCard'
import CardProfileHistory from '../../components/cardProfileHistory'
import dummy from '../../assets/icons/dummy.png'

function Community_Profile() {
	const usernameInput = useRef(null)
	const [avatar, setAvatar] = useState()
	const [update, setUpdate] = useState(0)
	const [loading, setLoading] = useState(false)
	const [tutorials, setTutorials] = useState([])
	const [profile, setProfile] = useState({})
	const [showNameInput, setShowNameInput] = useState(false)
	const [completeName, setCompleteName] = useState('')
	const [biography, setBiography] = useState('')
	const { username } = useParams()
	async function handleKeyDown(e) {
		if (e.key === 'Enter') {
			const token_data = jwt_decode(localStorage.getItem('@conig-Token'))
			try {
				await api
					.put(`/profile/${token_data.uid}`, {
						completeName,
						biography,
						informations: true
					})
					.then(response => response.data)
					.then(result => {
						setUpdate(update + 2)
						setShowNameInput(false)
					})
			} catch (error) {
				alert(`${error} Ocorreu um erro ao buscar os items`)
			}
		}
	}
	async function handleBlur(e) {
		const token_data = jwt_decode(localStorage.getItem('@conig-Token'))
		try {
			await api
				.put(`/profile/${token_data.uid}`, {
					completeName,
					biography,
					informations: true
				})
				.then(response => response.data)
				.then(result => {
					console.log(result)
					setUpdate(update + 1)
					setShowNameInput(false)
				})
		} catch (error) {
			alert(`${error} Ocorreu um erro ao buscar os items`)
		}
	}
	useEffect(() => {
		async function getProfile() {
			const token_data = jwt_decode(localStorage.getItem('@conig-Token'))
			try {
				const { data } = await api.get(`/profile/${username}`)
				setProfile(data)
				setCompleteName(data.complete_name)
				setBiography(data.biography)
				setTutorials(data.studentTutorial)
				setLoading(true)
			} catch (error) {
				alert(`${error} Ocorreu um erro ao buscar os items`)
			}
		}
		getProfile()
		setLoading(true)
	}, [update])

	async function handleAvatar(e) {
		e.preventDefault()
		const imagefile = document.querySelector('#image-input')
		setAvatar(imagefile.files[0])

		const formData = new FormData()
		formData.append('avatar', imagefile.files[0])
		const token_data = jwt_decode(localStorage.getItem('@conig-Token'))

		fetch(`https://api.conig.info/profile/${token_data.uid}`, {
			method: 'PUT',
			body: formData,
			headers: {
				authorization: `Bearer ${localStorage.getItem('@conig-Token')}`
			}
		})
			.then(response => response.json())
			.then(result => {
				console.log('Success:', result)
				setUpdate(update + 1)
			})
			.catch(error => {
				console.error('Error:', error)
			})
	}
	async function openInputName() {
		setShowNameInput(true)

		await usernameInput.current.focus()
	}
	function openFile() {
		document.getElementById('image-input').click()
	}

	return profile.studentTutorial ? (
		<div>
			<SiteHeader />
			<div className="main-container">
				<img
					src={
						profile.profile_pic
							? `https://api.conig.info/uploads/profile_pic/${profile.profile_pic}`
							: dummy
					}
					alt="profile-pic"
					id="profile-pic"
				/>

				<span
					className={`profile-username  ${
						showNameInput === true ? 'profile-none' : 'profile-display'
					}`}
				>
					{profile.complete_name}
				</span>

				<span className="profile-username__showname">@{profile.username}</span>
				<span className="profile-username__showname">Nivel {profile.level}</span>
				<hr className="profile-hr" />
				<div className="profile-user-informations">
					<label htmlFor="description">Sobre mim</label>
					<br />
					<span id="profile-user__description">{biography}</span>
					<br />
					Conquistas:
					{profile.studentCollectibles.length > 0 &&
						profile.studentCollectibles.map(collectible => (
							<ProfileCard achievement={collectible.collectible.description} />
						))}
				</div>
				<hr className="profile-vertical-hr" />
				<div className="profile-history">
					Tutoriais/Exercicios completados:
					{profile.studentTutorial.length > 0 &&
						profile.studentTutorial.map(tutorial => (
							<CardProfileHistory exercise={false} tutorial={tutorial.tutorial.title} />
						))}
					{profile.studentExercise.length > 0 &&
						profile.studentExercise.map(tutorial => (
							<CardProfileHistory exercise={tutorial.exercise.title} tutorial={false} />
						))}
				</div>
			</div>
		</div>
	) : (
		<></>
	)
}

export default Community_Profile
