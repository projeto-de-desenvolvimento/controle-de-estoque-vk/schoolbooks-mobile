import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router'
import SideNav from '../../components/sideNav'
import SiteHeader from '../../components/siteHeader'
import api from '../../services/api'
import './styles.css'

function Course() {
	const [module, setModule] = useState({})
	const [links, setLinks] = useState([])
	const [tutorial, setTutorial] = useState({})
	const { id } = useParams()

	useEffect(() => {
		async function getModule() {
			try {
				const { data } = await api.get(`/modules/${id}`)
				setModule(data.module)
				setLinks(data.module.classes)
				setTutorial(data.tutorial)
			} catch (err) {
				console.log(err)
			}
		}

		getModule()
	}, [])

	setTimeout(() => {
		window.dispatchEvent(new Event('resize'))
	}, 150)

	return (
		<div>
			<SiteHeader></SiteHeader>
			<main className="main-container">
				<SideNav data={links} module_name={module.name} module_id={module.id}></SideNav>
				<section className="content">
					<img src={module.module_image_link} className="content__image" />
					<h1 className="content__title">{module.name}</h1>
					<h1 className="content__subtitle">Bem vindo ao módulo {module.name}</h1>
					<span className="content__overview">{module.description}</span>
					<span className="content__ready">Está pronto para iniciar? Clique no botão abaixo </span>
					<a href={`/module/${id}/tutorial/${tutorial.id}`} className="content__ready--button">
						Iniciar módulo
					</a>
				</section>
			</main>
		</div>
	)
}

export default Course
